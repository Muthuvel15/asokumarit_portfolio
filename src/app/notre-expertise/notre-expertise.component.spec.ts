import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotreExpertiseComponent } from './notre-expertise.component';

describe('NotreExpertiseComponent', () => {
  let component: NotreExpertiseComponent;
  let fixture: ComponentFixture<NotreExpertiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotreExpertiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotreExpertiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
